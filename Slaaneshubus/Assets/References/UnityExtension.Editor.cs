﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Extensions.Unity.Editor
{
    public static class EditorExtension
    {
        public static ICollection<T> SaveCollection<T>(this UnityEditor.Editor e, string collectionName, string itemName, ICollection<T> collection, ref bool showItems, Func<T> createNew, Func<T, T> onDisplay)
        {
            return SaveCollection(e, collectionName, itemName, collection, true, ref showItems, createNew, onDisplay);
        }
        public static ICollection<T> SaveCollection<T>(this UnityEditor.Editor e, string collectionName, string itemName, ICollection<T> collection, ref bool showItems, Func<T, T> onDisplay)
        {
            return SaveCollection(e, collectionName, itemName, collection, ref showItems, () => default(T), onDisplay);
        }
        public static ICollection<T> SaveCollection<T>(this UnityEditor.Editor e, string collectionName, string itemName, ICollection<T> collection, bool canChangeLength, ref bool showItems, Func<T, T> onDisplay)
        {
            return SaveCollection(e, collectionName, itemName, collection, canChangeLength, ref showItems, () => default(T), onDisplay);
        }
        public static ICollection<T> SaveCollection<T>(this UnityEditor.Editor e, string collectionName, string itemName, ICollection<T> collection, bool canChangeLength, ref bool showItems, Func<T> createNew, Func<T, T> onDisplay)
        {
            List<T> result = new List<T>(collection);
            EditorGUILayout.BeginHorizontal();
            showItems = EditorGUILayout.Foldout(showItems, collectionName, true);
            EditorGUILayout.Space(5);
            if (canChangeLength)
            {
                int length = result.Count;
                length = EditorGUILayout.IntField("Length", length);
                if (GUILayout.Button("+"))
                    length++;
                if (GUILayout.Button("-"))
                    length--;
                if (length > result.Count){

                    for (int i = collection.Count; i < length; i++)
                        result.Add(createNew());
                }
                else if (length < result.Count)
                {
                    List<T> newResult = new List<T>();
                    for (int i = 0; i < length; i++)
                        newResult.Add(result[i]);
                    result = newResult;
                }
            }
            else EditorGUILayout.LabelField(result.Count.ToString());
            EditorGUILayout.EndHorizontal();
            if (showItems)
            {
                int length = result.Count;
                for (int i = 0; i < length; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(string.Format("{0} {1}", itemName, i));
                    EditorGUILayout.Space(-200);
                    result[i] = onDisplay(result[i]);
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                }
            }
            return result;
        }
        public static void SaveTiedCollections<T1, T2>(this UnityEditor.Editor e, string label, string description, string nameItemA, string nameItemB, ref List<T1> a, ref List<T2> b, bool canChangeLength, ref bool showItems, Func<T1> newItemA, Func<T2> newItemB, Func<T1, T1> onDisplayA, Func<T2, T2> onDisplayB)
        {
            List<T1> resultA = new List<T1>(a);
            List<T2> resultB = new List<T2>(b);
            EditorGUILayout.BeginHorizontal();
            showItems = EditorGUILayout.Foldout(showItems, new GUIContent(label, description));
            EditorGUILayout.Space(5);
            if (a.Count != b.Count)
            {
                Debug.LogWarning("Collections are not of the same length, creating new item in the smaller collection");
                if (a.Count > b.Count)
                    for (int i = resultB.Count; i < resultA.Count; i++)
                        resultB.Add(newItemB());
                else
                    for (int i = resultA.Count; i < resultB.Count; i++)
                        resultA.Add(newItemA());
            }
            if (canChangeLength)
            {
                int length = a.Count;
                length = EditorGUILayout.IntField("Length", length);
                if (GUILayout.Button("+"))
                    length++;
                if (GUILayout.Button("-"))
                    length--;
                if (length > a.Count)
                    for (int i = a.Count; i < length; i++)
                    {
                        resultA.Add(newItemA());
                        resultB.Add(newItemB());
                    }
                else if (length < a.Count)
                {
                    List<T1> newResultA = new List<T1>();
                    List<T2> newResultB = new List<T2>();
                    for (int i = 0; i < length; i++)
                    {
                        newResultA.Add(resultA[i]);
                        newResultB.Add(resultB[i]);
                    }
                    resultA = newResultA;
                    resultB = newResultB;
                }

            }
            else EditorGUILayout.LabelField(a.Count.ToString());
            EditorGUILayout.EndHorizontal();
            if (showItems)
            {
                int length = resultA.Count;
                for (int i = 0; i < length; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(nameItemA);
                    EditorGUILayout.Space(-500);
                    resultA[i] = onDisplayA(resultA[i]);
                    EditorGUILayout.LabelField(nameItemB);
                    EditorGUILayout.Space(-500);
                    resultB[i] = onDisplayB(resultB[i]);
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                }
            }
            a = resultA;
            b = resultB;
        }
    }
}
#endif