﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
class BootScreenManager : InterfaceManager
{
    public GameObject @lock;
    protected override void BootScreen()
    {
        base.BootScreen();

        //check if the user wanted to use a lock
        if (InstanceInfo.main.lockData.useLock)
        {
            //just in case use lock is set but the user didn't make a key
            if (InstanceInfo.main.lockData.lockSet)
            {
                @lock.SetActive(true);
                MakeLock(JsonConvert.DeserializeObject<LockData>((InstanceInfo.main.parsed)), false, () => { StartCoroutine(FadeLoadingScreen(true)); }, Quit);
                return;
            }
            InstanceInfo.main.lockData.useLock = false;
        }
        StartCoroutine(FadeLoadingScreen(true));
    }
}
