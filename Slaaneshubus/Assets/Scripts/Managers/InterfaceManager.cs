﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Newtonsoft.Json;
using Extensions;
/// <summary>
/// Manages all the data about the interface.
/// </summary>
public class InterfaceManager : MonoBehaviour
{
    public static List<ColorReference> colors;
    public static event Action<ColorableObjectType> OnColorChange = delegate { };
    public static InterfaceManager main;


    private Canvas canvas;
    private int sceneIndex;
    private int targetSceneIndex;
    public Slider loadProgressSlider;
    public CanvasGroup loadScreen;
    public float fadeSpeed;
    protected static float loadProgress;
    private bool sceneChanged;

    protected Action _Back = delegate { };
    protected Action _Update = delegate { };

    public event Action OnBack;

    protected virtual void BootScreen()
    {
        ReadInstanceInfo();
        targetSceneIndex = 1;
        StartCoroutine(ChangeScene(1, LoadSceneMode.Additive, () => SceneManager.GetSceneByBuildIndex(1).GetRootGameObjects().ForEach(x => x.SetActive(false))));
        _Update = BootUpdate;


    }

    protected virtual void MainMenuScreen()
    {
        ReadInstanceInfo();
    }



    /// <summary>
    /// Used for when user has to enter a key
    /// </summary>
    public static LockManager MakeLock(LockData key, bool dispose, params Action[] actions)
    {
        LockManager mgr = FindObjectOfType<LockManagerMarker>().Initialize();
        if (actions.Length > 0)
        {
            mgr.OnKeyOpen += actions[0];
            if (actions.Length > 1)
                mgr.OnKeyCancel += actions[1];
        }
        mgr.EnterKey(key, dispose);
        return mgr;

    }
    /// <summary>
    /// Used when user needs to make a new lock
    /// </summary>
    public static LockManager MakeLock(string cancelButtonText, bool dispose, Action<LockData> onSuccess, Action onCancel)
    {
        LockManager mgr = FindObjectOfType<LockManagerMarker>().Initialize();
        mgr.OnLockSet += onSuccess;
        mgr.OnLockCancel += onCancel;
        mgr.SetLock(cancelButtonText, dispose);
        return mgr;
    }




    private void Awake()
    {
        main = this;
        colors = new List<ColorReference>();
        for (int i = 0; i < Enum.GetNames(typeof(ColorableObjectType)).Length; i++)
            colors.Add(new ColorReference());
        canvas = this.GetComponent<Canvas>();
        sceneIndex = this.gameObject.scene.buildIndex;
        OnBack = delegate { };
        switch (sceneIndex)
        {
            case 0:
                BootScreen();
                break;
            case 1:
                MainMenuScreen();
                break;
        }


    }


    private void ReadInstanceInfo()
    {
        InstanceInfo.main = new InstanceInfo();
        //check if info file exists
        if (!FileManager.Exists(FileManager.InstanceInfoPath))
        {
            //if not, make one
            FileManager.Create(FileManager.InstanceInfoPath);
            FileManager.WriteToFile(FileManager.InstanceInfoPath, InstanceInfo.ToJson());
        }
        else
        {
            //if it does, read it
            InstanceInfo.FromJson(FileManager.ReadFromFile(FileManager.InstanceInfoPath));
            //and set the ui data correctly
            colors = new List<ColorReference>(InstanceInfo.main.colors);
        }
    }
    protected void SaveInfo() => FileManager.WriteToFile(FileManager.InstanceInfoPath, InstanceInfo.ToJson());


    protected virtual void Update() => _Update();
    private void BootUpdate()
    {
        if (sceneChanged && loadScreen.alpha == 1)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(targetSceneIndex));
            SceneManager.GetActiveScene().GetRootGameObjects().ForEach(x => x.SetActive(true));
            StartCoroutine(UnloadOldScene(sceneIndex));
            sceneChanged = false;

        }
    }

    public void Back()
    {
        _Back();
        OnBack();
    }

    public void ChangeColor(ColorableObjectType type, Color color)
    {
        colors[(int)type] = color;
        OnColorChange(type);
    }

    public void DiscardChanges()
    {
        //reset all the variables to the values of the InstanceInfo.main
        colors = new List<ColorReference>(InstanceInfo.main.colors);
        for (int i = 0; i < colors.Count; i++)
        {
            OnColorChange((ColorableObjectType)i);
        }

    }


    public void Toggle_UseLock(bool tog)
    {
        InstanceInfo.main.lockData.useLock = tog;
        if (tog && !InstanceInfo.main.lockData.lockSet)
        {
            LockManager mgr = FindObjectOfType<LockManager>();
            mgr = MakeLock("Back", true, s =>
             {
                 InstanceInfo.main.lockData = s;
                 InstanceInfo.main.lockData.lockSet = true;
                 _Back -= mgr.Back;
                 SaveInfo();
             }, () => { InstanceInfo.main.lockData.useLock = false; _Back -= mgr.Back; SaveInfo(); });
            _Back += mgr.Back;
            return;
        }
        SaveInfo();
    }
    public void ChangeLock()
    {
        LockManager mgr = FindObjectOfType<LockManager>();
        mgr = MakeLock("Back", true, s =>
         {
             InstanceInfo.main.lockData = s;
             _Back -= mgr.Back;
             SaveInfo();
         }, () => { _Back -= mgr.Back; SaveInfo(); });
        _Back += mgr.Back;

    }

    protected IEnumerator FadeLoadingScreen(bool fadeIn)
    {
        float change;
        float target;
        if (fadeIn)
        {
            change = fadeSpeed;
            target = 1f;
        }
        else
        {
            change = -fadeSpeed;
            target = 0f;
        }
        while (loadScreen.alpha != target)
        {
            loadScreen.alpha += change * Time.deltaTime;
            yield return null;
        }
    }


    private IEnumerator UnloadOldScene(int sceneIndex)
    {
        AsyncOperation op = SceneManager.UnloadSceneAsync(sceneIndex);
        while (!op.isDone)
            yield return null;
    }

    private IEnumerator ChangeScene(int sceneIndex, LoadSceneMode mode, Action onFinished)
    {
        loadProgressSlider.value = loadProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(sceneIndex, mode);
        while (!op.isDone)
        {
            //op.isDone will be true at around 90%. This clamp makes sure the bar does reach 100%
            loadProgress = Mathf.Clamp(op.progress / 0.9f, 0, 1);
            loadProgressSlider.value = loadProgress;
            yield return null;
        }
        loadProgressSlider.value = loadProgress = 1; ;
        onFinished();
        sceneChanged = true;

    }

    public void Save()
    {
        //Pass all info data to InstanceInfo.main
        InstanceInfo.main.colors = new List<ColorReference>(colors);
        SaveInfo();
    }
    public static void Quit()
    {
#if UNITY_EDITOR
        Debug.Log("Should quit");
#else
        Application.Quit();
#endif
    }

}
