﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
/// <summary>
/// Used for when the user needs to change locks or input keys
/// </summary>
public class LockManager : MonoBehaviour
{
    /// <summary>
    /// The text used to ask the user for input
    /// </summary>
    public TextMeshProUGUI inputRequest;
    /// <summary>
    /// Dropdown for selecting the lock type
    /// </summary>
    public LockSelector lockSelector;
    /// <summary>
    /// The current lock type
    /// </summary>
    public LockGameObject currentLock;
    /// <summary>
    /// The text object of the back button for canceling the operation
    /// </summary>
    public TextMeshProUGUI backButtonText;
    /// <summary>
    /// Used to not have to constantly perform if checks
    /// </summary>
    private Action onNext;
    /// <summary>
    /// Used to not have to constantly perform if checks
    /// </summary>
    private Action onPrev;
    /// <summary>
    /// The previous key entered. Used for checking if the key entered is correct
    /// </summary>
    public string previousKey;
    public event Action<LockData> OnLockSet;
    public event Action OnLockCancel;
    public event Action OnKeyCancel;
    public event Action OnKeyOpen;
    public event Action OnDestroy;
    /// <summary>
    /// The text to appear on the back button when it will cancel the operation
    /// </summary>
    private string cancelButtonText;
    /// <summary>
    /// Should all lock related object be removed when the operation completes or is aborted?
    /// </summary>
    private bool disposeOnExit;
    public Vector2 offset;
    public void Init()
    {
        OnLockSet = delegate { };
        OnLockCancel = delegate { };
        OnKeyOpen = delegate { };
        OnKeyCancel = delegate { };
        OnDestroy = delegate { };

    }
    /// <summary>
    /// Initializes a lock of the requested type
    /// </summary>
    /// <param name="type"></param>
    private void NewLock(LockType type)
    {
        currentLock = Instantiate(Resources.Load<LockGameObject>("UI/Prefabs/Locks/" + type.ToString()), this.transform);
        currentLock.transform.localPosition = offset;
        currentLock.onEnter.AddListener(Next);
    }
    public void OnLockChanged()
    {
        currentLock.onEnter.RemoveListener(Next);
        Destroy(currentLock.gameObject);
        NewLock(lockSelector.CurrentLock);
    }

    public void Next() => onNext();
    public void Back() => onPrev();

    public void EnterKey(LockData @lock, bool dispose)
    {
        disposeOnExit = dispose;
        previousKey = @lock.key;
        NewLock((LockType)@lock.type);
        inputRequest.text = $"Enter {currentLock.LockType.ToString()}";
        onPrev = OnKeyCancel;
        onNext = () => { if (CheckKey()) OnKeyOpen(); };
        if (dispose)
        {
            onNext += ExitFromKey;
            onPrev += ExitFromKey;
        }
    }
    private bool CheckKey()
    {
        if (previousKey != currentLock.input)
        {
            inputRequest.text = $"{currentLock.LockType.ToString()}s were not the same";
            currentLock.Clear();
            return false;
        }
        else return true;
    }

    /// <summary>
    /// Use the function when the user has to set a new lock. Remember to add Listeners to OnLockCancel and OnLockSet
    /// </summary>
    /// <param name="cancelButtonText">
    /// The text that should appear when the back button leads to canceling the operation
    /// </param>
    public void SetLock(string cancelButtonText, bool dispose)
    {
        this.cancelButtonText = cancelButtonText;
        disposeOnExit = dispose;
        //user needs to choose a lock and key
        inputRequest.text = "Select a new lock";
        //initialize the lock selecor
        lockSelector = Instantiate(Resources.Load<LockSelector>("UI/Prefabs/LockSelector"), this.transform);
        lockSelector.transform.localPosition = offset + new Vector2(0, 265);
        lockSelector.onValueChanged += OnLockChanged;
        //create a new lock
        NewLock(lockSelector.CurrentLock);
        //set delegates to their correct values
        onNext = ToConfirmLock;
        onPrev = OnLockCancel;
        if (disposeOnExit)
        {
            onPrev += ExitFromLock;
            Action a = delegate { };
            a = delegate
            {
                onPrev -= ExitFromLock;
                onNext -= a;
            };
            onNext += a;
        }

    }
    private void ToSetNewLock()
    {
        backButtonText.text = cancelButtonText;
        inputRequest.text = "Select a new lock";
        lockSelector.gameObject.SetActive(true);
        currentLock.Clear();
        onNext = ToConfirmLock;
        onPrev = OnLockCancel;
    }
    private void ToConfirmLock()
    {
        lockSelector.gameObject.SetActive(false);
        backButtonText.text = "Back";
        inputRequest.text = "Confirm " + lockSelector.CurrentLock.ToString();
        previousKey = currentLock.input;
        currentLock.Clear();
        onPrev = ToSetNewLock;
        onNext = () =>
        {
            if (CheckKey())
            {
                OnLockSet(new LockData() {lockSet = InstanceInfo.main.lockData.lockSet, useLock = InstanceInfo.main.lockData.useLock, type = this.lockSelector.CurrentIndex, key = previousKey });
                inputRequest.text = $"{lockSelector.CurrentLock.ToString()} saved";
            }
        };
        if (disposeOnExit)
        {
            OnLockSet += d => ExitFromLock();
            Action a = delegate { };
            a = delegate
             {
                 OnLockSet -= d => ExitFromLock();
                 onPrev -= a;
             };
            onPrev += a;
        }
    }
    private void ExitFromLock()
    {
        Destroy(lockSelector.gameObject);
        ExitFromKey();
    }
    private void ExitFromKey()
    {
        Destroy(currentLock.gameObject);
        Destroy(this);
        OnDestroy();
    }
}