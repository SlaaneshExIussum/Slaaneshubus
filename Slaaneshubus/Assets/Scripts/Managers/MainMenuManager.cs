﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
public class MainMenuManager : InterfaceManager
{
    public Toggle useLockToggle;
    private Transform currentActive;




    protected override void MainMenuScreen()
    {
        base.MainMenuScreen();
        useLockToggle.SetIsOnWithoutNotify(InstanceInfo.main.lockData.useLock);
        loadProgressSlider.value = loadProgress;
        loadScreen.alpha = 1;
        _Update = MainMenuUpdate;
        currentActive = this.transform.Find("MainMenu");
        _Back = Quit;
    }


    private void MainMenuUpdate()
    {
        if (loadScreen.alpha > 0)
            StartCoroutine(FadeLoadingScreen(false));
        else _Update = delegate { };
    }


    public void SwitchSceens(Transform changeTo)
    {
        if (!changeTo.parent.gameObject.activeSelf)
            changeTo.parent.gameObject.SetActive(true);
        currentActive.gameObject.SetActive(false);
        currentActive = changeTo;
        currentActive.gameObject.SetActive(true);
        CalculateBack(currentActive);
    }
    private void CalculateBack(Transform from)
    {
        if (from == this.GetComponent<Screen>().main)
            _Back = Quit;
        else if (from == from.GetComponentInParent<Screen>().main)
            _Back = () => SwitchSceens(from.parent.parent.GetComponent<Screen>().main);
        else
            _Back = () => SwitchSceens(from.parent.GetComponent<Screen>().main);
    }
}