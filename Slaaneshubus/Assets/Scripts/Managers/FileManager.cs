﻿using System;
using System.Text;
using System.IO;
using Extensions;
using UnityEngine;
public static class FileManager
{
    private static string _executingPath;
    public static string ExecutingPath => getExecPath();
    private static Func<string> getExecPath = ExecutingPathInit;
    private static string ExecutingPathInit()
    {
        //used only when the program was just booted to find the correct path
#if UNITY_ANDROID
        _executingPath = Application.persistentDataPath ;
#else
        _executingPath = (Directory.GetCurrentDirectory() + "\\");
#endif
        //afterwards, the executing path will not change, so we don't need to constantly find it
        getExecPath = GetExecutingPath;
        return _executingPath;
    }
    private static string GetExecutingPath() => _executingPath;
    /// <summary>
    /// The path where the Instance info should be located
    /// </summary>
    public static string InstanceInfoPath => _executingPath + "info.sbi";

    /// <summary>
    /// Does the file exist?
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static bool Exists(string path) => File.Exists(path);
    /// <summary>
    /// Creates a new File at the given path
    /// </summary>
    /// <param name="path"></param>
    public static void Create(string path) => File.Create(path).Close();
    /// <summary>
    /// Encrypts the given data
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string Encrypt(string data)
    {
        return Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
    }
    /// <summary>
    /// Encrypts the data and writes it to the specified file.
    /// If a file already exists, it will be deleted
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="data"></param>
    public static void WriteToFileEncrypted(string filePath, string data) => WriteToFile(filePath, Encrypt(data));
    /// <summary>
    /// Writes the given data to the specified file
    /// If a file already exists, it will be deleted
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="data"></param>
    public static void WriteToFile(string filePath, string data)
    {
        if (File.Exists(filePath))
            File.Delete(filePath);
        using (FileStream stream = File.OpenWrite(filePath))
        {
            stream.Position = 0;
            stream.Write(Encoding.UTF8.GetBytes(data));
            stream.Close();
        }

    }
    /// <summary>
    /// Retrieves all text from the file
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static string ReadFromFile(string filePath) => File.ReadAllText(filePath);
    /// <summary>
    /// Retrieves and decrypts the text from the file
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static string ReadFromEncryptedFile(string filePath) => Decrypt(ReadFromFile(filePath));
    /// <summary>
    /// Takes the data and decrypts it
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string Decrypt(string data)
    {
        string result = "";
        byte[] decrpyted = Convert.FromBase64String(data);
        decrpyted.ForEach(x => result += (char)x);
        return result;
    }
    /// <summary>
    /// Does the File exist? If so, read from it
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    public static bool ReadFromFile(string filePath, out string result)
    {
        result = "";
        if (!File.Exists(filePath))
            return false;
        result = ReadFromFile(filePath);
        return true;
    }
}