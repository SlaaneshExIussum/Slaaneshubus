﻿using System;
/// <summary>
/// Object that stores data on locks
/// </summary>
[Serializable]
public class LockData
{

    /// <summary>
    /// Did the user choose a lock and make a key?
    /// </summary>
    public bool lockSet;
    /// <summary>
    /// Does the user want the application the be locked?
    /// </summary>
    public bool useLock;
    public int type;
    public string key;
}