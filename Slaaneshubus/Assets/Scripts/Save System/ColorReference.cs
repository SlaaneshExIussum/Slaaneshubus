﻿using System;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Extensions;

/// <summary>
/// Class used for saving the referenece to a color variable, instead of constantly asking for it
/// </summary>
[Serializable]
public class ColorReference : Reference<Color>
{
    [JsonIgnore]
    public Color Color { get => _item; set { _item = value; } }
    public float r => _item.r;
    public float g => _item.g;
    public float b => _item.b;
    public float a => _item.a;
    [JsonConstructor]
    public ColorReference(float r, float g, float b, float a)
    {
        this._item = new Color(r, g, b, a);
    }
    public ColorReference(Color c) : base(c) { }




    public ColorReference() : base() { }
    public static implicit operator ColorReference(Color c) => new ColorReference(c);

}