﻿using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Extensions;
/// <summary>
/// Class containing initialization information
/// </summary>
[Serializable]
public class InstanceInfo
{
    [JsonIgnore]
    public static InstanceInfo main;
    public List<ColorReference> colors = new List<ColorReference>();
    [JsonIgnore]
    public LockData lockData = new LockData();
    //needs to be implemented
    public float UIScale;
    public string parsed;
    public InstanceInfo()
    {
        for (int i = 0; i < Enum.GetNames(typeof(ColorableObjectType)).Length; i++)
            colors.Add(new ColorReference());
    }
    public static void FromJson(string data)
    {
        JObject serialized = JObject.Parse(data);
        main = serialized.ToObject<InstanceInfo>();
        JToken colToken = serialized.SelectToken("colors");
        main.colors = colToken.Children().ForEach(x => x.ToObject<ColorReference>()).ToList();
        main.lockData = JsonConvert.DeserializeObject<LockData>(main.parsed);
    }
    public static string ToJson()
    {
        main.parsed = JsonConvert.SerializeObject(main.lockData);
        return JsonConvert.SerializeObject(main);
    }
}
