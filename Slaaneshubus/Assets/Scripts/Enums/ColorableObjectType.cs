﻿/// <summary>
/// The kinds of objects that can have their color changed
/// </summary>
public enum ColorableObjectType
{
    Button,
    Text,
    TextBackdrop,
    Backdrop,
    ButtonText,
    ExampleText,
    ProgressBar,
    ProgressBarBackdrop,
}