﻿public enum LockType
{
    Password,
    Pattern,
    Pin
}