﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Base class for identification objects
/// </summary>
public abstract class LockGameObject : MonoBehaviour
{
    /// <summary>
    /// The current user input
    /// </summary>
    public string input;
    /// <summary>
    /// The text object for showing the input
    /// </summary>
    public TextMeshProUGUI field;
    /// <summary>
    /// Called when the user has finished entering the key
    /// </summary>
    public UnityEvent onEnter;
    private Action onClear;
    private Action onDel;
    private Action<string> onInput;
    public abstract LockType LockType { get; }
    protected virtual void Start()
    {
        //check if the field variable is set
        //if not, we don't need to try to access it
        //we won't have to check if the variable is set every time 
        //one of the functions is called by using this method
        if (field != null)
        {
            onClear = ClearWithField;
            onDel = DelWithField;
            onInput = InputWithField;
            field.text = "";
        }
        else
        {
            onClear = ClearNoField;
            onDel = DelNoField;
            onInput = InputNoField;
        }
    }

    /// <summary>
    /// Clears all input
    /// </summary>
    public void Clear() => onClear();
    /// <summary>
    /// Deletes the last entered character
    /// </summary>
    public void Del() => onDel();
    /// <summary>
    /// Used to add more digits to the input
    /// </summary>
    /// <param name="s"></param>
    public void Input(string s) => onInput(s);
    protected virtual void ClearWithField()
    {
        ClearNoField();
        field.text = "";
    }
    protected virtual void DelWithField()
    {
        DelNoField();
        field.text = field.text.Length == 0 ? "" : field.text.Remove(field.text.Length - 1);
    }
    /// <summary>
    /// To be called when the user has finished entering the key
    /// </summary>
    public virtual void Enter()
    {
        onEnter.Invoke();
    }
    protected virtual void InputWithField(string s)
    {
        InputNoField(s);
        field.text += s;
    }
    protected virtual void InputNoField(string s) => input += s;
    protected virtual void ClearNoField() => input = "";
    protected virtual void DelNoField() => input = input.Length == 0 ? "" : input.Remove(input.Length - 1);

    public static implicit operator LockData(LockGameObject l) => new LockData() { key = l.input, type = (int)l.LockType };
}