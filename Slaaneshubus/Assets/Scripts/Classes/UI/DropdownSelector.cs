using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
using TMPro;
/// <summary>
/// Base class to make using dropdowns easier
/// </summary>
public class DropdownSelector : MonoBehaviour
{
    /// <summary>
    /// The actual dropdown object
    /// </summary>
    private TMP_Dropdown _dropdown;
    private List<string> _options = new List<string>();
    /// <summary>
    /// The options that will be shown by _dropdown
    /// </summary>
    public List<string> Options
    {
        get => _options;
        protected set
        {
            _options = value;
            _dropdown.options = new List<TMP_Dropdown.OptionData>(_options.ForEach(x => { return new TMP_Dropdown.OptionData(x); }));
        }
    }

    private int _currentIndex;
    /// <summary>
    /// The current index of the dropdown
    /// </summary>
    public int CurrentIndex
    {
        get => _currentIndex;
        set
        {
            _currentIndex = value;
            if (_currentIndex >= _options.Count)
                _currentIndex = 0;
            else if (_currentIndex < 0)
                _currentIndex = _options.Count - 1;
            _dropdown.value = _currentIndex;
            onValueChanged();
        }
    }
    /// <summary>
    /// Will be called when another option is selected
    /// </summary>
    public event Action onValueChanged;
    protected virtual void Awake()
    {
        _dropdown = this.GetComponentInChildren<TMP_Dropdown>();
        _dropdown.onValueChanged.AddListener(OnValueChanged);
        onValueChanged = delegate { };
    }
    /// <summary>
    /// Used when the value changes through the actual dropdown object
    /// </summary>
    /// <param name="i"></param>
    private void OnValueChanged(int i){_currentIndex = i; onValueChanged(); }
    /// <summary>
    /// Choose the next option (Used by the buttons attached to the GameObject)
    /// </summary>
    public virtual void Next() => CurrentIndex++;
    /// <summary>
    /// Choose the previous option (Used by the buttons attached to the GameObject)
    /// </summary>
    public virtual void Previous() => CurrentIndex--;

}
