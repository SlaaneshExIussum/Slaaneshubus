﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Class used for setting the theme of the UI
/// </summary>
public class ColorableObject : MonoBehaviour
{
    public Graphic targetGraphic;
    public ColorableObjectType type;
    /// <summary>
    /// Used to reference the correct color from the InterfaceManager
    /// </summary>
    private void Start()
    {

        RefreshColor(this.type);
        InterfaceManager.OnColorChange += RefreshColor;

    }
    private void RefreshColor(ColorableObjectType type)
    {
        if (this.type == type)
            try
            {
                targetGraphic.color = InterfaceManager.colors[(int)type];
            }
            catch (Exception e)
            {
                Debug.Log(this.name);
                string path = $".{this.name}";
                Transform current = this.transform;
                while (current.parent != null)
                {
                    path.Insert(0, $".{current.name}");
                    current = current.parent;
                }
                Debug.LogWarning($"{path} threw out: {e}");
            }
    }
    private void OnDestroy()
    {
        InterfaceManager.OnColorChange -= RefreshColor;
    }
}