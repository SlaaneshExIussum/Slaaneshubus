using UnityEngine;
using UnityEngine.Events;
using TMPro;
public class PinLock : LockGameObject
{
    public int maxDigits;
    public override LockType LockType { get => LockType.Pin; }
    protected override void InputWithField(string s)
    {
        if (input.Length < maxDigits)
        {
            input += s;
            field.text += '*';
        }
    }


    public void ShowPin()
    {
        field.text = input;
    }
    public void HidePin()
    {
        field.text = "";
        for (int i = 0; i < input.Length; i++)
        {
            field.text += '*';
        }
    }


}