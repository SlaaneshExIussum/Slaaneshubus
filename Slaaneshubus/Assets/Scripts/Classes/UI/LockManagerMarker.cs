﻿using UnityEngine;
using TMPro;
/// <summary>
/// Script used for marking object that can have a LockManager attached to them.
/// References must be set.
/// </summary>
public class LockManagerMarker : MonoBehaviour
{
    public TextMeshProUGUI inputRequest;
    public TextMeshProUGUI backButtonText;
    public Vector2 offset;
    public LockManager Initialize()
    {
        gameObject.AddComponent(typeof(LockManager));
        LockManager result = this.GetComponent<LockManager>();
        result.Init();
        result.offset = offset;
        result.backButtonText = backButtonText;
        result.inputRequest = inputRequest;
        inputRequest.gameObject.SetActive(true);
        result.OnDestroy += () => InterfaceManager.main.OnBack += DeactivateTextOnScreenChange;
        return result;
    }
    private void DeactivateTextOnScreenChange()
    {
        inputRequest.gameObject.SetActive(false);
        InterfaceManager.main.OnBack -= DeactivateTextOnScreenChange;
    }
}