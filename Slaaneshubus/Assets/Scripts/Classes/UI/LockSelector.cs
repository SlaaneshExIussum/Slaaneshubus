﻿using System.Linq;
using System.Collections.Generic;
using Extensions;
public class LockSelector : DropdownSelector
{
    public LockType CurrentLock { get => locks[CurrentIndex]; }
    List<LockType> locks = new List<LockType>();

    protected override void Awake()
    {
        base.Awake();
        List<string> options = new List<string>();
        locks.Add(LockType.Password);
        locks.Add(LockType.Pattern);
        locks.Add(LockType.Pin);
        Options = locks.ForEach(x => { return x.ToString(); }).ToList();
    }

}