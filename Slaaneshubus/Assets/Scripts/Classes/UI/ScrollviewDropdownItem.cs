﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class ScrollviewDropdownItem : MonoBehaviour
{
    [Header("Items to be dropped down")]
    public List<GameObject> items = new List<GameObject>();
    protected Action OnDropdown;
    protected void Start()
    {
        OnDropdown = OnDropdown_Activate;

    }
    public void UseDropdown()
    {
        OnDropdown();
    }
    protected void OnDropdown_Activate()
    {
        int i = 0;
        int thisIndex = this.transform.GetSiblingIndex()+1;
        items.ForEach(x =>
        {
            x.SetActive(true);
            x.transform.SetParent(this.transform.parent);
            x.transform.SetSiblingIndex(thisIndex + i);
            i++;
        });
        OnDropdown = OnDropdown_Deactivate;
    }
    protected void OnDropdown_Deactivate()
    {
        items.ForEach(x =>
        {
            x.SetActive(false);
            x.transform.SetParent(this.transform);
        });
        OnDropdown = OnDropdown_Activate;

    }
}