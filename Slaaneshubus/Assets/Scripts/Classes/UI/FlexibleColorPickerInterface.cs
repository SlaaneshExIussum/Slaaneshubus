using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlexibleColorPickerInterface : MonoBehaviour
{
    public ColorableObjectType type;
    private FlexibleColorPicker picker;
    public void Awake()
    {
        picker = this.GetComponent<FlexibleColorPicker>();
        picker.color = InterfaceManager.colors[(int)type];
        InterfaceManager.OnColorChange += x =>
        {
            if (x == type)
                picker.SetColor_Silent(InterfaceManager.colors[(int)x]);
        }; 
    }

    public void OnColorChange()
    {
        InterfaceManager.main.ChangeColor(type, picker.color);
    }
}
